"""
Autor : Gabriel Jean
Description : This is a little script I wrote to sort the content on my Plex Media Server
I'm still fairly new to this so if you have any ideas feel free to help !
date : 2018-03-31

"""


import os, shutil, requests, json, re


def ask():
    rep = str(input("What type of content do you want to sort ?\n[Movie | TV | Animes | Videos] : "))
    lang = str(input("Now Please Select à Language :\n[EN | FR] : "))
    print("Alright ! You selected : " + rep.lower() + " in " + lang.lower())

def sort(sub3):
    r = requests.get("http://www.omdbapi.com/?t=" + sub3 + "&apikey=e8cb9f9a")
    data = r.json()
    rep = data['Response']
    if rep == "True":
        type = data['Type']
        print(type)

def sorty(sub3,year):
    r = requests.get("http://www.omdbapi.com/?t=" + sub3 + "&y=" + year + "&apikey=e8cb9f9a")
    data = r.json()
    rep = data['Response']
    if rep == "True":
        type = data['Type']
        print(sub3, type)
        
def main():
    file = []

    for root, dirs, files in os.walk("G:\\plex\\Animes", topdown=True):

        for name in files:
            file.append(os.path.join(root, name))


    for movie in file:
        head, name = os.path.split(movie)
        sub1 = re.sub(r'.* ', '', name)

        sub2 = re.sub('(?:.2018|.2017|.2016|.2015|.2014|.2013|.2012|.2011|.2010|.2009|.2008|.S0|.1080p|.720p).*', '', sub1)

        sub3 = sub2.replace(".", "+")

        try:
            match = re.search('2\d{3}', sub1)
            year = match.group()
            sorty(sub3, year)
        except:
            print("We cannot found the year !")
            sort(sub3)

if __name__ == '__main__':
    #ask()
    main()

